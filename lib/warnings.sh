warn_node_modules() {
  local modules_source=$1
  if [ "$modules_source" == "prebuilt" ]; then
    warning "node_modules checked into source control" "https://www.npmjs.org/doc/misc/npm-faq.html#should-i-check-my-node_modules-folder-into-git-"
  elif [ "$modules_source" == "" ]; then
    warning "No package.json found"
  fi
}

warn_build() {
  local build_method=$1
  if [ "$build_method" == "" ]; then
    warning "No npm build script or gulp build task found"
  fi
}
