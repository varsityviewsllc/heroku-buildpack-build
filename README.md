# Heroku Buildpack for Post-Install Build

A build pack to install and cache node devDependencies (like heroku-buildpack-nodejs) and execute a post-install build script.

If you fork this repository, please **update this README** to explain what your fork does and why it's special.


## How it Works

This buildpack assumes the node buildpack (and bower buildpack) run before this one. This build pack begins with an `npm install` of devDependencies.  When the devDependencies have been installed, one of two supported build methods is used:

1. 'npm run build', or
2. 'gulp build', if there is no npm build script in the package.json

After the build command completes, the node_modules are cached (again with the devDependencies). If NODE_ENV="production", node_modules are then pruned back for production.

For technical details, check out the compile script.

## Documentation

For more information about using Node.js and buildpacks on Heroku, see these Dev Center articles:

- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/nodejs)
- [Buildpacks](https://devcenter.heroku.com/articles/buildpacks)
- [Buildpack API](https://devcenter.heroku.com/articles/buildpack-api)


## Options

### Use npm or gulp to do a post-install build

Add a "build" script in package.json -or- a gulpfile.js file with a "build" task to the project root.
